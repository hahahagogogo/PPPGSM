
/*********************************************************************************
*      Copyright:  (C) 2022 guoyi
*                  All rights reserved.
*
*       Filename:  main.c
*    Description:  This file
*
*        Version:  1.0.0(07/01/2021)
*         Author:  guoyi <675088383@qq.com>
*      ChangeLog:  1, Release initial version on "22/5/21 11:21:32 AM"
*
********************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <unistd.h>
#include <stdlib.h>
#include <getopt.h>
#include <pthread.h>
#include <ctype.h>
#include <libgen.h>
#include <semaphore.h>


#include "ini_proc.h"
#include "iniparser.h"
#include "dictionary.h"
#include "comport.h"
#include "atcmd.h"
#include "ppp.h"
#include "ping.h"
#include "gsm.h"
#include "logger.h"
#include "tty_check.h"
#include "parse_xml.h"
#include "util_proc.h"


#define DAEMON_PIDFILE             "/tmp/.ppp.pid"
#define PROG_VERSION               "v1.0.0"

typedef void *(THREAD_BODY)(void *thread_arg);


enum
{
	PPP_DISCONNECTED,       /*  0, 未拨号 */
	PPP_CONNECTED,          /*  1, 拨号完成 */
};

enum
{
	PPP_STOP,               /*  0表示停止拨号 */
	PPP_START,              /*  1表示开始拨号 */
};


int     g_sig_start_ppp = 1;
int     g_stop = 0;
sem_t   sem;


int thread_start(pthread_t * thread_id, THREAD_BODY * thread_workdy, void *thread_arg);
int check_set_program_running(int background, char *pidfile);
void *thread_worker(void *ctx);
void install_signal();


void sig_handler(int signum)
{
	switch (signum)
	{
		/*  g_sig_start_ppp 为手动拨号和断开的标志, 1表示拨号 */
	case SIGUSR1:   // 信号10
		log_nrml(" g_sig_start_ppp = 1...................\n");
		g_sig_start_ppp = 1;
		break;

		/*  g_sig_start_ppp 为手动拨号和断开的标志, 0表示断开拨号 */
	case SIGUSR2:   // 信号12
		log_nrml(" g_sig_start_ppp = 0...................\n");
		g_sig_start_ppp = 0;
		if (1 == check_iface_exist("ppp10"))
		{
			stop_ppp();
		}
		break;

	case SIGINT:    // 信号2
	case SIGTERM:   // 信号15
		g_stop = 1;
		break;
	}
	return;
}

static void program_usage(char *progname)
{

	printf("Usage: %s [OPTION]...\n", progname);
	printf(" %s is pppd daemon program running on ARM\n", progname);

	printf("\nMandatory arguments to long options are mandatory for short options too:\n");
	printf(" -d[debug   ]  Running in debug mode\n");
	printf(" -c[conf    ]  Specify configure file\n");
	printf(" -h[help    ]  Display this help information\n");
	printf(" -v[version ]  Display the program version\n");

	printf("\n%s version %s\n", progname, PROG_VERSION);
	return;
}


int main(int argc, char *argv[])
{
	int                 rv = -1;
	logger_t            logger;
	comport_t           comport;
	pthread_t           tid;
	ppp_ctx_t           ppp_ctx;
	conf_t             *conf;
	regist_t           *regist;
	apn_t              *apne;
	mod_hwinfo_t       *mod_hwinfo;
	char               *p = NULL;
	int                 reg = 0;
	int                 found_tty = 0;
	int                 rc = -1;
	char               *conf_file = "./etc/4g.conf";
	int                 opt = 0;
	int                 background = 1;
	int                 debug = 0;
	char               *progname = NULL;
	int                 num = 0;

	struct option long_options[] = {
		{ "conf", required_argument, NULL, 'c' },
		{ "debug", no_argument, NULL, 'd' },
		{ "version", no_argument, NULL, 'v' },
		{ "help", no_argument, NULL, 'h' },
		{ NULL, 0, NULL, 0 }
	};

	progname = (char *)basename(argv[0]);

	/* Parser the command line parameters */
	while ((opt = getopt_long(argc, argv, "c:dvh", long_options, NULL)) != -1)
	{
		switch (opt)
		{
		case 'c': /* Set configure file */
			conf_file = optarg;
			break;

		case 'd': /* Set debug running */
			background = 0;
			debug = 1;
			break;

		case 'v':  /* Get software version */
			printf("%s version %s\n", progname, PROG_VERSION);
			return 0;

		case 'h':  /* Get help information */
			program_usage(progname);
			return 0;

		default:
			break;
		}

	}

	memset(&ppp_ctx, 0, sizeof(ppp_ctx));
	conf = &ppp_ctx.conf;
	regist = &ppp_ctx.regist;
	apne = &conf->apne;
	mod_hwinfo = &ppp_ctx.mod_hwinfo;

	install_signal();

	rv = ini_parser(conf_file, &ppp_ctx);
	if (rv < 0)
	{
		printf("Configuration file parsing failure rv = : %d\n", rv);
		return -1;
	}

	if (check_set_program_running(background, DAEMON_PIDFILE) < 0)
	{
		return -1;
		goto OUT;
	}

	/*  第二个参数为0, 表示该信号在进程的线程之间共享 */
	sem_init(&sem, 0, 1);

	if (thread_start(&tid, thread_worker, (void *)&ppp_ctx) < 0)
	{
		log_err("Start PPP thread failure....\n");
		return -2;
	}
	log_nrml("Start PPP thread success....\n");


	while (!g_stop)
	{
		/*  找tty设备 , 找到了设置标志位, 并且打开最后一个串口, 供主线程使用, */
		if (!found_tty)
		{
			log_dbg("The name of the serial port to look for is : %s\n", conf->found_modname);
			num = find_tty(conf->found_modname, ppp_ctx.devname);
			if (num > 1)
			{
				if (comport_open(&comport, ppp_ctx.devname[num - 1], conf->baudrate, "8n1n") < 0)
				{
					log_err("open serial port :%s Failed\n", ppp_ctx.devname[num - 1]);
					goto NEXTLOOP;
				}
				if (atcmd_set_echo(&comport, 0) < 0)
				{
					log_err("Send ate0 failed\n");
					goto NEXTLOOP;
				}
				log_nrml("open serial port [%s] successfully\n", ppp_ctx.devname[num - 1]);
				found_tty = 1;
			}
			else
			{
				log_nrml("There are less than two serial devices, find again\n");
				goto NEXTLOOP;
			}
		}
		else
		{
			if (check_module_pullout(ppp_ctx.devname, num) != num)      /* 判断是否有被拔出  */
			{
				log_nrml("The serial port you are looking for was not found\n");
				found_tty = 0;
				goto NEXTLOOP;
			}
			else
			{
				log_nrml("The serial port to be found has not disappeared\n");
				found_tty = 1;
			}
		}

		log_nrml("Start checking module status\n");
		rv = at_module_ready(&comport, &ppp_ctx);      /*  检查模块状态 sim_check_module()  获取硬件信息 在此函数中*/
		if (rv < 6)
		{
			log_err("Module does not meet PPP dialing standards....\n");    /*  模块不符合PPP拨号标准 */
			goto NEXTLOOP;
		}
		log_nrml("module status compliant with PPP dialing standards:%d....\n", rv);     /*  模块状态符合PPP拨号标准 */

		rv = check_nic_priority(conf);   /*  据网卡的优先级和硬件的条件判断是否需要拨号上网 */
		if (rv == 1)
		{
			/*   此时为4G未拨号, 并且有线和无线网卡也没有网络, 则需要启动4G拨号
			*   (此处不直接置拨号的标志, 因为拨号也受手动信号控制)             */
			log_nrml("No network on any NIC, ppp dial-up required\n");
		}
		else if (rv == 2)
		{
			/*   此时为4G拨号成功, 并且有线或无线网卡恢复上网, 则需要断开4G拨号 */
			ppp_ctx.action = 0;
			log_nrml("At this time ppp dial-up is connected, wired or wireless network card to restore the Internet, disconnect ppp dial-up\n");
		}
		else
		{
			/*   此时4G和(有线或无线)就有一个正在上网, 则4G拨号状态不做改变     */
			log_nrml("At this time 4G or wired wireless network, do not change\n");
		}

		if (g_sig_start_ppp && rv == 1 && regist->status == STATUS_IMSIOK)
		{
			log_nrml("All the conditions for dial-up Internet access are met, Set the logo....\n");     /*  符合所有拨号上网的条件, 设置标志 */
			ppp_ctx.action = 1;
		}
		else
		{
			log_err("Does not satisfy the conditions for dial-up Internet access ....\n");     /*  不满足需要PPP拨号上网条件 */
			ppp_ctx.action = 0;
		}
	NEXTLOOP:
		sleep(5);
	}

	sem_wait(&sem);

	sem_post(&sem);
	log_nrml("The child thread has exited, the main thread has exited\n");

	comport_close(&comport);
OUT:
	log_close();

	return 0;
}



int check_set_program_running(int background, char *pidfile)
{
	if (check_daemon_running(DAEMON_PIDFILE))
	{
		log_err("Program already running, process exit now\n");
		return -1;
	}

	if (background)
	{
		if (set_daemon_running(DAEMON_PIDFILE) < 0)
		{
			log_err("set program running as background failure\n");
			return -2;
		}
	}
	else
	{
		if (record_daemon_pid(DAEMON_PIDFILE) < 0)
		{
			log_err("record program running PID failure\n");
			return -3;
		}
	}

	return 0;
}

/*安装信号*/
void install_signal()
{
	struct sigaction act, oldact;

	act.sa_handler = sig_handler;

	sigaddset(&act.sa_mask, SIGQUIT);

	act.sa_flags = 0;
	sigaction(SIGINT, &act, &oldact);
	sigaction(SIGCONT, &act, &oldact);
	sigaction(SIGUSR1, &act, &oldact);
	sigaction(SIGUSR2, &act, &oldact);
}

/*线程开始*/
void *thread_worker(void  *args)
{
	int             rv = -1;
	ppp_ctx_t      *ppp_ctx = (ppp_ctx_t *)args;
	regist_t       *regist = &ppp_ctx->regist;
	conf_t         *conf = &ppp_ctx->conf;
	int             found_apn = 0;

	sem_wait(&sem);

	while (!g_stop)
	{
		if (ppp_ctx->ppp_status == PPP_CONNECTED)
		{
			log_nrml("Dialing is in the finished state....\n");     /*  拨号处于完成状态 */
			if (PPP_STOP == ppp_ctx->action)
			{
				stop_ppp();
				log_nrml("Dialing needs to be stopped....\n");        /*  拨号停止 */
				ppp_ctx->ppp_status = PPP_DISCONNECTED;
			}
		}
		else    /*  处于拨号未连接状态 */
		{
			log_nrml("Dial-up is not connected....\n");     /*  拨号处于未连接状 */
			if (PPP_START != ppp_ctx->action)
			{
				log_nrml("Network is connected, no dial-up required....\n");     /*  此时有网络, 不需要拨号 */
			}
			else        // if( PPP_START == ctx->action )
			{
				/*  自动获取apn  */
				rv = automatically_get_apn(ppp_ctx);
				if (rv < 0)
				{
					continue;
				}
				else
				{
					log_nrml("apn has been found, no need to continue searching\n");
				}

				log_nrml("Network not connected, need dial-up....\n");    /*  此时无网络, 需要拨号 */
				/*  如果拨号完成, 把ppp_status 状态赋值为 PPP_CONNECTED */
				if (0 == start_ppp(ppp_ctx, "rasppp", PPP_FILE))    /*fork() and execl拨号脚本*/
				{
					ppp_ctx->ppp_status = PPP_CONNECTED;
					log_nrml("PPP dial-up success....\n");      /*  PPP拨号成功, 并设置标志 */
				}
				else
				{
					ppp_ctx->ppp_status = PPP_DISCONNECTED;
					log_nrml("PPP dial-up failure....\n");      /*  PPP拨号失败, 设置标志, 重新开始拨号 */
				}
			}
		}
		sleep(10);
	}

	if (1 == check_iface_exist("ppp10"))
	{
		stop_ppp();
		log_nrml("The program is closed, at this time 4G dial-up needs to be disconnected\n");
	}

	sem_post(&sem);
	log_nrml("The child thread has been safely exiten\n");
}


int thread_start(pthread_t * thread_id, THREAD_BODY * thread_workdy, void *thread_arg)
{
	int             rv = -1;
	pthread_attr_t  thread_attr;

	if (pthread_attr_init(&thread_attr))
	{
		log_err("Pthread_attr_init() failure: %s\n", strerror(errno));
		goto CleaUp;
	}

	if (pthread_attr_setstacksize(&thread_attr, 120 * 1024))
	{
		log_err("pthread_attr_setstacksize() failure: %s\n", strerror(errno));
		goto CleaUp;
	}

	if (pthread_attr_setdetachstate(&thread_attr, PTHREAD_CREATE_DETACHED))
	{
		log_err("thread_attr_setdetachstate() failure: %s\n", strerror(errno));
		goto CleaUp;
	}

	if (pthread_create(thread_id, &thread_attr, thread_workdy, thread_arg))
	{
		log_err("Create thread failure: %s\n", strerror(errno));
		goto CleaUp;
	}

	rv = 0;

CleaUp:
	pthread_attr_destroy(&thread_attr);
	return rv;
}