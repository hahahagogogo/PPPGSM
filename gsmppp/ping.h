/********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  ping.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(17/07/21)
 *         Author:   guoyi<675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/05/15 09:14:50"
 *                 
 ********************************************************************************/



#ifndef __PING_H__
#define __PING_H__

#include <stdio.h>
#include <stddef.h>
#include <string.h>

#include "ini_proc.h"

extern int check_nic_priority(conf_t *conf);
extern int ping_test(char *iface, char *ping_addr);
extern int check_iface_exist(char *iface);
extern int check_iface_ipaddr(char *iface, char *ipaddr, int size);


#endif 
