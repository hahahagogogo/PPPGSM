/*********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  ppp.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(23/05/21)
 *         Author:  guoyi <675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/05/26 15:02:38"
 *                 
 ********************************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ppp.h"
#include "ping.h"
#include "atcmd.h"
#include "comport.h"
#include "ini_proc.h"
#include "parse_xml.h"

int start_ppp(ppp_ctx_t *ppp_ctx, char *script_name, char *PPP_FILE)
{
	
	pid_t 		pid1;
	int 		ppp_file_fd=-1;

	if ((ppp_file_fd = open(PPP_FILE, O_RDWR | O_CREAT | O_TRUNC, 0644)) < 0)
	{
		log_error("Redirect PPPD program standard output to file failure: %s\n", strerror(errno));
		return -1;
	}
	pid1 = fork();  //fork系统调用执行ppp脚本
	if (pid1 < 0)
	{
		log_error("fork() create child process failure: %s\n", strerror(errno));
		return -2;
	}
	else if (pid1 == 0) //子进程开始运行
	{
		log_info("Child process start execute pppd program\n");

		dup2(ppp_file_fd, STDOUT_FILENO);

		//if( execl("/bin/sudo", "sudo", "pppd", "call", "rasppp", NULL) == -1 )
		if (execl("/bin/sudo", "sudo", "pppd", ppp_ctx->devname, "call", script_name, NULL) == -1)
		{
			log_error(" execl 'sudo pppd %s call rasppp' error. \n", ppp_ctx->devname);
			return -3;
		}
	}
	log_nrml("Get the ip of ppp10, ppp dial up successfully\n");
	return 0;
}


//int start_ppp(ppp_ctx_t *ppp_ctx)
//{
//    int         rv = -1;
//    FILE        *fp;
//    char        cmd[512];
//    char        ipaddr[64];
//    char        buf[512];
//    int         remaining = 512;
//
//    memset(cmd, 0, sizeof(cmd));
//    memset(buf, 0, sizeof(buf));
//    memset(ipaddr, 0, sizeof(ipaddr));
//
//#if 0
//    if( ppp_ctx->apne.uid != NULL && NULL != ppp_ctx->apne.pwd )
//    {
//        snprintf(cmd, 128, "ifup-ppp -d %s -a %s -u %s -p %s ppp10", ppp_ctx->devname[0], ppp_ctx->apne.apn, ppp_ctx->apne.uid, ppp_ctx->apne.pwd);
//    }
//    else
//    {
//        snprintf(cmd, 128, "ifup-ppp -d %s -a %s ppp10", ppp_ctx->devname[0], ppp_ctx->apne.apn);
//    }
//#endif 
//
//    snprintf(cmd, 128, "ifup-ppp -d %s -a %s ppp10", ppp_ctx->devname[0], ppp_ctx->apne.apn);
//
//    log_nrml("pppd: %s\n", cmd);
//    fp = popen(cmd, "r");
//
//    log_nrml("Start ppp dial-up, wait 20s\n");
//    sleep(20);
//
//    rv = check_iface_exist("ppp10");
//    if( rv < 0 )
//    {
//        log_err("No ppp10 network card found\n");
//        return -1;
//    }
//    log_nrml("ppp10 network card exists\n");
//
//    rv = check_iface_ipaddr("ppp10", ipaddr, sizeof(ipaddr));
//    if( rv < 0 )
//    {
//        log_err("Didn't get the ip of ppp10, ppp dial-up failure\n");
//        return -2;
//    }
//    log_nrml("Get the ip of ppp10, ppp dial up successfully\n");
//
//    return 0;
//}


int stop_ppp(void)
{
    char        cmd[128];
    int         remaining = 128;

    memset(cmd, 0, sizeof(cmd));

    strncat(cmd, "killall pppd", remaining);
    system(cmd);

    return 0;
}

int automatically_get_apn(ppp_ctx_t *ppp_ctx)
{
    int             rv = -1;
    conf_t          *conf;
    regist_t        *regist;

    conf = &ppp_ctx->conf;
    regist = &ppp_ctx->regist;
#if 1 

    if( !regist->found_apn )
    {
        if( strcmp(conf->apne.apn, "\0") != 0 )
        {
            log_nrml("The apn is already read in the configuration file, no need to automatically find the apn\n");
            strcpy(ppp_ctx->apne.apn, conf->apne.apn);
            log_nrml("The apn of dial-up internet is : %s\n", ppp_ctx->apne.apn);
            regist->found_apn = 1;
        }
        else
        {
            log_nrml("apn not read in config file, need to find mcc, mnc\n");
            rv = query_apn_xml(regist->mcc, regist->mnc, &conf->apne);
            if( rv < 0 )
            {
                log_err("Failed to query apn, return value is: %d\n", rv);
                return -1;
                //continue;
            }
            log_nrml("The apn query was successful, and the apn is: %s\n", conf->apne.apn); 
            strcpy(ppp_ctx->apne.apn, conf->apne.apn);
            regist->found_apn = 1;
        }
    }
    else
    {
        log_nrml("apn has been found, no need to continue searching\n");
    }
#endif 

    return 0;

}
