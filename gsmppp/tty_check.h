/********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  atcmd.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(09/05/21)
 *         Author:   guoyi<675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/04/27 14:26:51"
 *                 
 ********************************************************************************/

#ifndef __TTY_CHECK_H__
#define __TTY_CHECK_H__

#include <stdio.h>
#include <stddef.h>
#include <string.h>

#include "comport.h"

#define BAUDRATE    115200
#define AT_MODEL    "AT+CGMM\r\n"
#define MAX_TTY     5
#define TTY_DEVNAME 32 

extern int find_tty(char  *module_name, char module_tty[MAX_TTY][TTY_DEVNAME]);
extern int check_module_pullout(char module_tty[MAX_TTY][TTY_DEVNAME], int number);


#endif 

