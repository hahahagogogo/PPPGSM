/********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  ping.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(17/07/21)
 *         Author:  iot <xxqq.com>
 *      ChangeLog:  1, Release initial version on "22/05/25 09:14:50"
 *                 
 ********************************************************************************/



#ifndef __PPP_H__
#define __PPP_H__

#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include "ini_proc.h"
#define PPP_FILE "/tmp/.ppp.log"
#define APN_FILE "/home/nbiot/guoyi/4G/apns-full-conf.xml"

extern int start_ppp(ppp_ctx_t *ppp_ctx, char *execl_name, char *PPP_FILE);
extern int stop_ppp();
extern int automatically_get_apn(ppp_ctx_t *ppp_ctx);



#endif 
