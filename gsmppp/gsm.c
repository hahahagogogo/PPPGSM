#include "gsm.h"
#define CB
/*********************************AT 回OK*******************************************/
int atcmd_set_at(comport_t *comport, int echo)
{
    char        buf[128];
    char        at[32];

    memset(buf, 0, sizeof(buf));
    memset(at, 0, sizeof(at));

    sprintf(at, "AT\r\n", echo);

    if( send_atcmd(comport, at, buf, sizeof(buf), 300) < 0 )
    {
        printf("send_atcmd AT failure!!!!!\n");
        return -2;
    }
#ifdef CB
	printf("send_atcmd AT OK~~~~~\n");
#endif

    return 0;
}

/****************************，查询是否有卡，模块是否需要密码******************/
int atcmd_check_cpin(comport_t *comport)
{
	char        buf[512];
	char        rbuf[512];

	memset(buf, 0, sizeof(buf));
	memset(rbuf, 0, sizeof(buf));

	if (send_atcmd(comport, "AT+CPIN?\r", buf, sizeof(buf), 300) < 0)
	{
		printf("send_atcmd AT+CPIN? failure!\n");
		return -1;
	}
	printf("send_atcmd AT+CPIN? ok~~~~~~~~~~`\n");
	if (at_fetch(buf, "\r\n", "\r\n", rbuf, sizeof(rbuf)))
	{
		printf("at_fetc no SIM detected!!!!!\n");//无SIM卡
		return -2;
	}
	printf("at_fetc have SIM ~~~~~~~~~~~\n")
	/*READY:MT is not pending for any password*/
	if (strstr(rbuf, "READY") == NULL)
	{
		printf("SIM not READY!!!!!\n");
		return 1;
	}
	printf("SIM have READY ~~~~~~~~~~~\n")
	return 0;
}
/******************************AT+CSQ\r\n 信号质量报告 *********************************/
int atcmd_check_rssi(comport_t *comport)                            /*  AT+CSQ  */
{
	char    buf[512];
	char    rbuf[128];
	memset(buf, 0, sizeof(buf));
	memset(rbuf, 0, sizeof(rbuf));
	if (send_atcmd(comport, AT_RSSI, buf, sizeof(buf), 300) < 0)
	{
		printf("send_atcmd AT+CSQ failure!!!!!!\n");
		return -1;
	}
	printf("send_atcmd AT+CSQ ok~~~~~~~~~~~~\n");
	if (at_fetch(buf, ":", ",", rbuf, 128) != 0)
	{
		printf("at_fetch AT+CSQ failure!!!!!!\n");
		return -2;
	}
	printf("at_fetch AT+CSQ ok~~~~~~~~~~~`\n");
	printf("AT+CSQ rbuf: %d\n", atoi(rbuf));
	return atoi(rbuf);
}

/****************************AT+CREG?\r用于网络注册状态，检测是否有服务******************/
int atcmd_check_atcreg(comport_t *comport)
{
	char    buf[512];
	char    rbuf[128];
	memset(buf, 0, sizeof(buf));
	memset(rbuf, 0, sizeof(rbuf));

	if (send_atcmd(comport, "AT+CREG?\r", buf, sizeof(buf), 300) < 0)
	{
		printf("send_atcmd AT+CREG? failure!!!!!!\n");
		return -1;
	}
	printf("send_atcmd AT+CREG? ok~~~~~~~~~~~~~~~~\n");
	if (at_fetch(buf, ":", "\r\n", rbuf, sizeof(rbuf)) != 0)
	{
		printf("at_fetch AT+CREG failure!!!!! \n");
		return -2;
	}
	printf("at_fetch AT+CREG ok~~~~~~~~ \n");
	if (strstr(rbuf, "0,1") == NULL && strstr(rbuf, "0,5") == NULL)
	{
		printf("AT+CREG no 1 or 5 !!!!!\n");
		return 1;
	}
	return 0;
}



/******************************AT+CGMI\r\n 获取制造商信息****************************/
int atcmd_check_manufacturer(comport_t *comport, char *module)
{
    char        buf[512];
    char        rbuf[512];

    memset(buf, 0, sizeof(buf));
    memset(rbuf, 0, sizeof(rbuf));

	/*TA returns manufacturer identification text.TA返回制造商标识文本：Quectel*/
	/*Maximum Response Time:300ms*/
    if( send_atcmd(comport, AT_MANFACT, buf, sizeof(buf),300) < 0 )
    {
        printf("send_atcmd AT+CGMI failure!\n");
        return -1;
    }
    if( at_fetch(buf, "\r\n", "\r\n", module, 512) != 0 )
    {
        printf("at_fetch AT+CGMI failure!!!!!\n");
        return -2;
    }
    return 0;
}

/*************************AT+CGMM\r\n  获取识别(型号)*********************************/
int atcmd_check_information(comport_t *comport, char *module)
{
    char    buf[512];
    char    rbuf[128];
    char    *ptr = NULL;

    memset(buf, 0, sizeof(buf));
    memset(rbuf, 0, sizeof(rbuf));

	/*TA returns product model identification text.TA返回产品型号标识文本如:EC20F*/
	/*Maximum Response Time:300ms*/
    if( send_atcmd(comport,AT_INFORMAT,buf, sizeof(buf), 300) < 0 )
    {
        printf("send_atcmd AT+CGMM failure!!!!!!\n");
        return -1;
    }
    if( at_fetch(buf,"\r\n","\r\n", module, 128) != 0)
    {
        printf("at_fetch AT+CGMM failure!!!!!\n");
        return -2;
    }

#ifdef CB
	printf("at_fetch AT+CGMM OK~~~~~~~~~~\n");
#endif

    return 0;
}

/***********************AT+CGMR\r\n  获取软件版本修订标识*****************************/
int atcmd_check_software_version(comport_t *comport, char *module)
{
    char    buf[512];
    memset(buf, 0, sizeof(buf));

	/*TA returns identification text of product software version.TA返回产品软件版本的标识文本。*/
	/*Maximum Response Time:300ms*/
    if( send_atcmd(comport, AT_SOFT, buf, sizeof(buf), 300) < 0 )
    {
        printf("send_atcmd AT+CGMR failure!\n");
        return -1;
    }

    if( at_fetch(buf, "\r\n", "\r\n", module, 128) != 0)
    {
        printf("at_fetch AT+CGMR failure\n");
        return -2;
    }

    return 0;
}

/***********************AT+CGSN\r\n  国际移动设备标识(IMEI)*****************************/
int atcmd_check_serial_number(comport_t *comport, char *module)
{
    char    buf[512];
    memset(buf, 0, sizeof(buf));

	/*AT+CGSN returns International Mobile Equipment Identity (IMEI).返回国际移动设备标识(IMEI)。*/
	/*Maximum Response Time:300ms*/
    if( send_atcmd(comport, AT_SER_NUM, buf, sizeof(buf), 300) < 0 )
    {
        printf("send_atcmd AT+CGSN failure!\n");
        return -1;
    }
    if( at_fetch(buf, "\r\n", "\r\n", module, 128) != 0)
    {
        printf("at_fetch AT+CGSN failure\n");
        return -2;
    }
#ifdef CB
	printf("at_fetch IMEI OK~~~~~~~~~~\n");
#endif
    return 0;
}

/******************************AT+CIMI\r\n  识别单个SIM连接*********************************/
int atcmd_check_user_identifier(comport_t *comport, char *module)
{
    char    buf[512];
    memset(buf, 0, sizeof(buf));

	/*TA returns <IMSI> for identifying the individual SIM which is attached to ME.TA返回用于识别单个SIM给我连接，如460023210226023*/
	/*Maximum Response Time:300ms*/
    if( send_atcmd(comport, AT_SERIAL, buf, sizeof(buf), 300) < 0 )
    {
        printf("send_atcmd AT+CIMI failure!\n");
        return -1;
    }
    if( at_fetch(buf, "\r\n", "\r\n", module, 128) != 0)
    {
        printf("at_fetch AT+CIMI failure\n");
        return -2;
    }
    return 0;
}

 
//
//
///*********************  AT+QNWINFO ****************************** */
//int atcmd_check_mccmnc(comport_t *comport, char *mcc, char *mnc)             
//{
//    char    buf[512];
//    char    rbuf[128];
//    char    *ptr = NULL;
//
//    memset(buf, 0, sizeof(buf));
//    memset(rbuf, 0, sizeof(rbuf));
//
//    if( send_atcmd(comport, "AT+QNWINFO\r", buf, sizeof(buf), 300) < 0 )
//    {
//        printf("send_atcmd AT+QNWINFO failure!\n");
//        return -1;
//    }
//
//    if( at_fetch(buf, ",\"", "\",", rbuf, 128) != 0 )
//    {
//        printf("Not find mcc and mnc \n");
//        return -2;
//    }
//
//    ptr = rbuf;
//    strncpy(mcc, ptr, 3);
//    ptr += 3;
//    strncpy(mnc, ptr, 3);
//
//    return 0;
//}
//
//
//int at_module_ready(comport_t *comport, ppp_ctx_t *ppp_ctx)
//{
//    regist_t        *regist = &ppp_ctx->regist;
//    mod_hwinfo_t    *mod_sjinfo = &ppp_ctx->mod_hwinfo;
//    int             rv = -1;
//    int             reg = -1;
//    int             csq = -1;
//    int             count = 0;
//
//    switch(regist->status)
//    {
//        case STATUS_UNKNOWN:
//            rv = atcmd_set_at(comport, 0);    /*  AT..............OK  */
//            if ( rv )
//            {
//                printf("rv = %d\n", rv);
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_ATOK:
//            rv = get_mod_sjinfo(comport, mod_hwinfo);
//            if ( rv )
//            {
//                printf("rv = %d\n", rv);
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_MODULE:
//            rv = atcmd_check_cpin(comport);
//            if ( rv )
//            {
//                printf("rv = %d\n", rv);
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_SIMOK:
//            csq = atcmd_check_rssi(comport);
//            if ( csq == 99 || csq < 3 )
//            {
//                printf("csq: %d\n", csq);
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_CSQOK:
//            reg = atcmd_check_atcreg(comport);
//            if ( reg )
//            {
//                printf("reg = %d\n", reg);
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_REGOK:
//            rv =  atcmd_check_mccmnc(comport, regist->mcc, regist->mnc);                            /*  AT+QNWINFO  */
//            if( rv < 0 )
//            {
//                printf("find mcc and mnc failure\n");
//                goto OUT;
//            }
//            regist->status ++;
//
//        case STATUS_IMSIOK: 
//            csq = atcmd_check_rssi(comport);
//            regist->csq_rssi=csq;
//
//            sleep(3);
//    }
//    return regist->status;
//
//OUT:
//
//    regist->status = 0;
//    return regist->status;
//}


/*  获取硬件信息, 把发AT，收AT封装到一起,只需要调用一次即可 */
int get_mod_sjinfo(comport_t *comport, mod_hwinfo_t *mod_hwinfo)
{
    atcmd_check_manufacturer(comport, mod_hwinfo->manufacturer);     /*  AT+CGMI */

    atcmd_check_information(comport, mod_hwinfo->module_name);      /*  AT+CGMM */

    atcmd_check_software_version(comport, mod_hwinfo->sfver);      /*  AT+CGMR */
    
    atcmd_check_serial_number(comport, mod_hwinfo->imei);       /*  AT+CGSN */

    atcmd_check_user_identifier(comport, mod_hwinfo->cimi);    /*  AT+CIMI */

    return 0;
}

