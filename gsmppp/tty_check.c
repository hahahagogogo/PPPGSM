/*********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  tty_check.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(27/07/21)
 *         Author:   guoyi<675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/04/24 14:47:40"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>

#include "comport.h"
#include "atcmd.h"
#include "gsm.h"
#include "tty_check.h"

#define PATH "/dev"

int find_tty(char  *module_name, char module_tty[MAX_TTY][TTY_DEVNAME])
{
    DIR             *pDir = NULL;
    struct dirent   *pEnt = NULL;
    int             i = 0;
    int             rc = 0;
    char            devname[64];
    char            buf[512];
    comport_t       comport;


    memset(buf, 0, sizeof(buf));
    memset(devname, 0, sizeof(devname));

    pDir = opendir(PATH);
    if (NULL == pDir)
    {
        perror("opendir");
        return -1; 
    }

    while( (pEnt = readdir(pDir)) ) 
    {

       if(strstr(pEnt->d_name, "ttyUSB") || strstr(pEnt->d_name, "ttyS"))
        { 
            snprintf(devname, 64, "%s/%s", PATH, pEnt->d_name);
            if( comport_open(&comport, devname, BAUDRATE, "8N1N") != 0 )
            {
                continue;
            }

            atcmd_set_echo(&comport, 0);

            //rc = send_atcmd(&comport, AT_MODEL, buf, 512, 300);

            rc = atcmd_check_information(&comport, buf);
            if( rc == 0 )
            {
                if( strstr(buf, module_name) )
                {
                    strcpy(module_tty[i++], devname);
                    comport_close(&comport);
                    sleep(1);
                }
            }
            else
            {
                comport_close(&comport);
                continue;
            }

            if( i >= 2 )
            {
                comport_close(&comport);
                return i;
            }

            comport_close(&comport);
        }
        else
        {
            continue;
        }
        comport_close(&comport);
    }
    closedir(pDir);

    return i;

}

int check_module_pullout(char module_tty[MAX_TTY][TTY_DEVNAME], int number)
{
    DIR             *pDir = NULL;
    struct dirent   *pEnt = NULL;
    int             i = 0;
    int             count = 0;
    char            devname[64];
    comport_t       comport;

    memset(devname, 0, 64);

    pDir = opendir(PATH);
    if (NULL == pDir)
    {
        perror("opendir");
        return -1; 
    }

    while( pEnt = readdir(pDir) )
    {
        snprintf(devname, 64, "%s/%s", PATH, pEnt->d_name);
        for(i=0; i<number; i++)
        {

            if( strstr(devname, module_tty[i]) )
            {
                i++;
                count++;
            }
        }
    }

    closedir(pDir);

    if( count != number )
    {
        return -1;
    }
    else
    {
        return count;
    }

    return count;

}

