/********************************************************************************
 *      Copyright:  (C) 2022 guoyi
 *                  All rights reserved.
 *
 *       Filename:  ping.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(17/07/21)
 *         Author:  guoyi<675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/04/25 09:14:50"
 *                 
 ********************************************************************************/

#ifndef __MODULE_STATUS_H__
#define __MODULE_STATUS_H__

#include <stdio.h>
#include <stddef.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <libgen.h>
#include <sys/ioctl.h>
#include <signal.h>

//#include "ini_proc.h"
#include "atcmd.h"
#include "comport.h"
typedef struct mod_hwinfo_s
{
	char    manufacturer[128];         //制造商,                 AT+CGMI
	char    module_name[128];          //模型识别(型号),		 AT+CGMM 
	char    sfver[128];				   //软件版本修订标识,       AT+CGMR
	char    imei[128];                 //IMEI号(手机序列号),     AT+CGSN
	char    cimi[128];				  //国际移动用户识别码,      AT+CIMI	 
} mod_hwinfo_t;
#define AT_MANFACT  "AT+CGMI\r\n"         //获取制造商信息
#define AT_INFORMAT "AT+CGMM\r\n"         //获取识别(型号) 
#define AT_SOFT     "AT+CGMR\r\n"         //获取软件版本修订标识
#define AT_SER_NUM  "AT+CGSN\r\n"         //获取IMEI号
#define AT_SERIAL   "AT+CIMI\r\n"         //获取国际移动用户标识码IMSI
#define AT_RSSI     "AT+CSQ\r\n"          //信号强度 

int atcmd_set_echo(comport_t *comport, int echo);                   /*  ATEX    */
int atcmd_check_manufacturer(comport_t *comport, char *module);     /*  AT+CGMI 获取制造商信息*/
int atcmd_check_information(comport_t *comport, char *module);      /*  AT+CGMM 获取识别(型号) */
int atcmd_check_software_version(comport_t *comport, char *module); /*  AT+CGMR 获取软件版本修订标识*/
int atcmd_check_serial_number(comport_t *comport, char *module);    /*  AT+CGSN 获取设备身份IMEI号 */
int atcmd_check_user_identifier(comport_t *comport, char *module);  /*  AT+CIMI 申请国际移动电话用户身份(IMSI)*/
int atcmd_check_rssi(comport_t *comport);							/*  AT+CSQ  信号质量报告*/

int atcmd_check_mccmnc(comport_t *comport, char *mcc, char *mnc);

int get_mod_hwinfo(comport_t *comport, mod_hwinfo_t *mod_hwinfo);




#endif 
