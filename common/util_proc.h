/********************************************************************************
 *      Copyright:  (C) 2022 guoyi 
 *                  All rights reserved.
 *
 *       Filename:  util_proc.h
 *    Description:  This head file is for Linux process/thread API
 *
 *        Version:  1.0.0(7/07/2021)
 *         Author:  guoyi <675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "7/06/2012 09:21:33 PM"
 *                 
 ********************************************************************************/

#ifndef __UTIL_PROC_H_
#define __UTIL_PROC_H_

#include <signal.h>

#define PID_ASCII_SIZE  11

/* excute a linux command by system() */
extern void exec_system_cmd(const char *format, ...);

/* check program already running or not from $pid_file  */
extern int check_daemon_running(const char *pid_file);

/* set program daemon running and record pid in $pid_file  */
extern int set_daemon_running(const char *pid_file);

/* record proces ID into $pid_file  */
extern int record_daemon_pid(const char *pid_file);

/* stop program running from $pid_file  */
extern int stop_daemon_running(const char *pid_file);

/* get daemon process ID from $pid_file   */
extern pid_t get_daemon_pid(const char *pid_file);

#endif
