/*********************************************************************************
 *      Copyright:  (C) 2022 guoyi 
 *                  All rights reserved.
 *
 *       Filename:  atcmd.c
 *    Description:  This file 
 *                 
 *        Version:  1.0.0(09/05/21)
 *         Author:  guoyi <675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/04/10 14:23:00"
 *                 
 ********************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>

#include "comport.h"
#include "logger.h"

int send_atcmd(comport_t *comport, char *atcmd, char *buf, int buf_size, int timeout)
{
    char    *ptr;
    int     rv = -1;
    int     bytes = 0;
    char    rbuf[1024];         //用来读取内核之前存放的内容
 
    fd_set          rfds;
    struct timeval  time_out;
    struct timeval  *time;

    if( !comport || !atcmd || !buf || buf_size <= 0 )
    {
        printf("%s, invalid parameter.\n", __func__);
        return -1;
    }

    comport_recv(comport, rbuf, sizeof(buf), timeout);
    memset(rbuf, 0, sizeof(rbuf));

    comport_send(comport, atcmd, strlen(atcmd));

    memset(buf, 0, buf_size);
    rv = comport_recv(comport, buf, buf_size, timeout);

    log_dbg("atcmd:%s, buf:%s\n",atcmd, buf);
  
    return rv;
}

/***************************************************************************************************************
 * 功能:
 * 如果传入atreply, start_key, end_key, buf   : 再传入字符串中找到start_key与end_key之间的字符串返回出去  
 * 如果传入atreply, start_key, end_key, NULL  : 则负责查找该区间的字符串, 不将找到的字符串返回出去
 * 如果传入atreply, start_key, NULL, NULL     : 则只负责查找start_key这个字符串
 *
 * 参数:
 * atreply      :   要解析的字符串
 * start_key    :   需要返回字符串的前一个或多个索引
 * end_key      :   需要返回字符串的后一个或多个索引
 * buf          :   返回的字符串
 * size         :   防止溢出
 *
 * 返回值:
 * rv = 0 成功;     rv <0 失败;     rv = -1 输入参数不正确;     rv = -2 没找到start_key;    rv = -3 没找到end_key
 *
 *************************************************************************************************************/ 

int at_fetch(char *atreply, char *start_key, char *end_key, char *rbuf, int size)
{
    if( !atreply || !start_key)
    {
        return -1;
    }

    char        *rbuf_stat;     //返回字符串的首地址
    char        *rbuf_end;      //返回字符串的尾地址
    int         rv = -1;

    if( rbuf != NULL )
    {
        memset(rbuf, 0, sizeof(*rbuf));
    }

    rbuf_stat = strstr(atreply, start_key);   //字符串查找

    if( rbuf_stat == NULL )
    {
        printf("strstr start_key :%s failure!!!!!!!!!!!!!\n", start_key);
        return -2;
    }
    rbuf_stat += strlen(start_key);

    if( end_key != NULL )
    {
        rbuf_end = strstr(rbuf_stat, end_key);
        if( rbuf_end == NULL )
        {
            printf("strstr end_key %s failure\n", rbuf_end);
            return -3;
        }
    }
    
    if( rbuf != NULL && ((rbuf_end-rbuf_stat) <= size) )
    {
        strncpy(rbuf, rbuf_stat, rbuf_end-rbuf_stat);
    }

    return 0;
}

