#ifndef __COMPORT_H__
#define __COMPORT_H__

#define DEVNAME_LEN 64     //串口名称长度64

/*变量结构体*/
typedef struct comport_s
{
	char            devname[DEVNAME_LEN];  //串口名称
	int               fd;         	//串口文件描述符
	long            baud_rate; 	 //波特率
	int               frag_size;  	//单次最大发送长度
	unsigned char   start_bits;         //起始位
	unsigned char   data_bits;  	    //数据位
	unsigned char   parity_bits;      //奇偶校验位
	unsigned char   stop_bits;       //停止位
	unsigned char   flowctrl_bits;   //空闲位流控
}comport_t;

/* ***************串口打开以及初始化********** */
extern int comport_open(comport_t *comport, char *devname, long baudrate, char *settings);
/*数据发送*/
extern int comport_send(comport_t *comport, char *data, int bytes);
/*接受数据*/
extern int comport_recv(comport_t *comport, char *buf, int size, int timeout);
/*串口关闭*/
extern int comport_close(comport_t *comport);

#endif 