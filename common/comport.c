#include <stdio.h>
#include <unistd.h>
#include <termios.h>  /*POSIX 终端控制定义*/	
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>	     /*文件控制定义*/

#include "comport.h"

/* ************************************************************串口初始化及打开**************************************************** */
int comport_open(comport_t *comport, char *devname, long baudrate, char *settings)
{
	int             i;
	int             rd;  		      //检查是否阻塞
	struct termios  new_termios;     //新端口配置，termios 函数族提供了一个常规的终端接口，用于控制非同步通信端口。简单来说就是通过这个结构体来对串口进行新的配置。
	if (!comport || !devname || !baudrate || !settings)
	{
		printf("invalid parameter\n");
		return -1;
	}


	/* *********************************************************初始化串口********************************************* */
	memset(comport, 0, sizeof(*comport));
	/* 串口名字赋值到结构体变量devname中 */
	strcpy(comport->devname, devname);
	/* 将文件描述符初始化为-1 */
	comport->fd = -1;
	/* 串口波特率赋值到结构体变量baud_rate中 */
	comport->baud_rate = baudrate;
	/* 单次最大发送长度 */
	comport->frag_size = 128;
	/* 8n1n 分别代表 串口的数据位, 奇偶校验位, 停止位, 串口流控 */
	/* 串口conf(8n1n)分别赋值到结构体变量中 */
	comport->data_bits = settings[0];      //数据位8位
	comport->parity_bits = settings[1];	//奇偶效验位n
	comport->stop_bits = settings[2];	//停止位1
	comport->flowctrl_bits = settings[3];	//空闲位n


	/***********************************************************打开文件I/O************************************************/
	/*  O_RDWR读写
	O_NOCTTY 在打开TTY设备文件时，阻止操作系统将打开的文件指定为进程的控制终端
	* O_NONBLOCK 使read处于非阻塞模式，不可阻断方式打开，无论是否有数据或等待，都立刻返回进程中
	*/
	comport->fd = open(devname, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (comport->fd < 0)
	{
		printf("%s, Open %s failed:%s\n", __func__, comport->devname, strerror(errno));
		return -2;
	}

	/* *******************************************************检测串口是否是阻塞状态********************************** */
	/*函数原型int fcntl(int fd, int cmd, long arg)， 根据文件描述词cmd来操作文件fd的特性，F_SETFL设置文件锁*/
	if (rd = fcntl(comport->fd, F_SETFL, 0) < 0)
	{
		printf("%s, %s,Fcntl check failed\n", __func__, strerror(errno));
		return -3;
	}
	printf("Starting serial communication process\n");



	/* ******************************************判断文件描述词是否是为终端机，函数原型isatty(fd)***************************/
	/* 如果文件为终端机则返回1, 否则返回0.*/
	if (0 == isatty(comport->fd))
	{
		printf("%s:[%d] is not a terminal equipment \n", comport->devname, comport->fd);
		return -4;
	}
	printf("Open %s successfully\n", comport->devname);


	/* *****************************获取终端的相关参数，函数原型int tcgetattr(int fd, struct termios *termios_p);******************** */
	///*  struct termios
	//{
	//unsigned short c_iflag; /* 输入模式标志*/
	//unsigned short c_oflag; /* 输出模式标志*/
	//unsigned short c_cflag; /* 控制模式标志，指定终端硬件控制信息*/
	//unsigned short c_lflag; /*区域模式标志或本地模式标志或局部模式*/
	//unsigned char c_line; /*行控制line discipline */
	//unsigned char c_cc[NCC]; /* 控制字符特性*/
	/*******************************************初始化c_cflag 控制模式标志***************************/
	/* 修改控制模式, 保证程序不占用串口 ，设置CLOCAL，modem的控制线将会被忽略。如果没有设置，则open()函数会阻塞直到载波检测线宣告modem处于摘机状态为止。*/
	new_termios.c_cflag |= (CLOCAL);

	/* 启动接收器, 能从串口中读取数据，设置CREAD，接收字符 */
	new_termios.c_cflag |= CREAD;

	/* CSIZE字符长度大小掩码, 将于设置databits相关的标志位置为0 */
	new_termios.c_cflag &= ~CSIZE;

	/*   cflag控制选项，回显等掩码
	* ICANON:  标准模式
	* ECHO:      回显输入的字符
	* ECHOE:    如果在标准模式下设定了ECHOE标志，则当收到一个ERASE控制符时将删除前一个显示字符
	* ISIG:        如果设置, 与INTR、QUIT和SUSP相对应的信号SIGINT、SIGQUIT和SIGTSTP会发送到tty设备的前台进程组中的所有进程.
	*/
	new_termios.c_cflag &= ~(ICANON | ECHO | ECHOE | ISIG);


	/***********************************************初始化iflag输入模式标志********************************/
	/*  iflag输入掩码
	* BRKINT:  没有设置IGNBRK而设置了BRKINT,中断条件清空输入输出队列中所有的数据并且向tty的前 台进程组中所有进程发送一个SIGINT信号
	* ICRNL:   如果设置，但IGNCR没有设置，接收到的回车符向应用程序发送时会变换成换行符
	* INPCK:   如果设置，则进行奇偶校验。如果不进行奇偶检验，PARMRK和IGNPAR将对存在的奇偶校验错误不产生任何的影响。
	* ISTRIP:  如果设置，所接收到的所有字节的高位将会被去除，保证它们是一个7位的字符。
	* IXON:    如果设置，接收到^S后会停止向这个tty设备输出，接收到^Q后会恢复输出。
	* */
	new_termios.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);


	/*********************************************初始化oflag输出模式标志******************************/
	/* OPOST:   表示处理后输出, 按照原始数据输出 */
	new_termios.c_oflag &= ~(OPOST);

	/***********************************************波特率设置****************************************/
	/*原始函数int cfsetospeed(struct termios *termptr, speed_t speed);
	参数：struct termios *termptr 指向termios结构的指针  speed_t speed 为需要设置的输出波特率*/
	switch (comport->baud_rate)
	{
	case 115200:
		cfsetispeed(&new_termios, B115200);	//输入波特率设置
		cfsetospeed(&new_termios, B115200);	//输出波特率设置
		break;
	case 57600:
		cfsetispeed(&new_termios, B57600);
		cfsetospeed(&new_termios, B57600);
		break;
	case 38400:
		cfsetispeed(&new_termios, B38400);
		cfsetospeed(&new_termios, B38400);
		break;
	case 19200:
		cfsetispeed(&new_termios, B19200);
		cfsetospeed(&new_termios, B19200);
		break;
	case 9600:
		cfsetispeed(&new_termios, B9600);
		cfsetospeed(&new_termios, B9600);
		break;
	case 4800:
		cfsetispeed(&new_termios, B4800);
		cfsetospeed(&new_termios, B4800);
		break;
	case 2400:
		cfsetispeed(&new_termios, B2400);
		cfsetospeed(&new_termios, B2400);
		break;
	}

	/***************************************************设置数据位************************************/
	/*字符长度，取值范围为CS5、CS6、CS7或CS8*/
	switch (comport->data_bits)
	{
	case '5':
		new_termios.c_cflag |= CS5;
		break;
	case '6':
		new_termios.c_cflag |= CS6;
		break;
	case '7':
		new_termios.c_cflag |= CS7;
		break;
	case '8':
		new_termios.c_cflag |= CS8;
		break;

	default:
		new_termios.c_cflag |= CS8; //默认数据位为8
		break;
	}

	/* ************************************************设置校验方式********************************** */
	switch (comport->parity_bits)
	{
		/* 无校验 */
	case 'n':
	case 'N':
		new_termios.c_cflag &= ~PARENB;   //控制选项使用奇偶校验掩码，为非奇偶校验
		new_termios.c_iflag &= ~INPCK;	      //输入选项使用奇偶校验掩码，为不允许奇偶校验
		break;
		/* 奇校验 */
	case 'e':
	case 'E':
		new_termios.c_cflag |= PARENB;      //控制选项为允许奇偶校验
		new_termios.c_cflag &= ~PARODD;//对输入奇偶校验，输出偶校验掩码，即不输入允许奇偶校验，不允许偶校验
		new_termios.c_iflag |= INPCK;         //输入为允许奇偶校验
		break;
		/* 偶校验 */
	case 'o':
	case 'O':
		new_termios.c_cflag |= PARENB;
		new_termios.c_cflag |= PARODD; //对输入奇偶校验，输出偶校验掩码，
		new_termios.c_iflag |= INPCK;
		/* 设置为空格 */
	case 's':
	case 'S':
		new_termios.c_cflag &= ~PARENB;
		new_termios.c_cflag &= CSTOPB; //设置两个停止位
		/* 默认无检验 */
	default:
		new_termios.c_cflag &= ~PARENB;
		new_termios.c_iflag &= ~INPCK;
		break;
	}

	/***********************************************设置停止位********************************************** */
	switch (comport->stop_bits)
	{
	case '2':
		new_termios.c_cflag |= CSTOPB;      //设置两个停止位，否则为1
		break;

	default:
		new_termios.c_cflag &= ~CSTOPB; //停止位为1，则要清楚CSTOPB
		break;
	}

	/* ******************************************设置串口流控 ****************************************/
	switch (comport->flowctrl_bits)
	{
	case 'S':               //软件流控
	case 's':
		new_termios.c_cflag &= ~(CRTSCTS);
		new_termios.c_cflag |= (IXON | IXOFF);
		break;

	case 'H':               //硬件流控
	case 'h':
		new_termios.c_cflag |= CRTSCTS;
		new_termios.c_cflag &= ~(IXON | IXOFF);
		break;

	case 'B':               //软件流控
	case 'b':
		new_termios.c_cflag &= ~(CRTSCTS);
		new_termios.c_cflag |= (IXON | IXOFF);
		break;

	case 'N':               //none
	case 'n':
		new_termios.c_cflag &= ~(CRTSCTS);
		new_termios.c_iflag &= ~(IXON | IXOFF);
		break;

	default:
		new_termios.c_cflag &= ~(CRTSCTS);
		new_termios.c_cflag &= ~(IXON | IXOFF);
		break;
	}

	/* ******************************************设置等待时间 ****************************************/
	new_termios.c_cc[VTIME] = 0;    //非规范模式读取时的超时时间，最长等待时间
	new_termios.c_cc[VMIN] = 2;     //最小接受字符
if (tcgetattr(comport->fd, &new_termios))
{
	printf("%s, get termios to new_termios failure:%s\n", __func__, strerror(errno));
	return -3;
}



/* ***************************************清空用于串口通信的输入输出缓冲区 ****************************************/
/*函数原型
tcflush函数刷清（扔掉）输入缓存（终端驱动法度已接管到，但用户法度尚未读）或输出缓存（用户法度已经写，但尚未发送）.
int tcflush（int filedes，int quene）
quene数该当是下列三个常数之一:
*TCIFLUSH  刷清输入队列
*TCOFLUSH  刷清输出队列
*TCIOFLUSH 刷清输入、输出队列
*/
if (tcflush(comport->fd, TCIFLUSH))
{
	printf("%s, failed to clear the cache:%s\n", __func__, strerror(errno));
	return -4;
}

/********************************** 设置串口属性, 除了查看波特率的函数波特率的函数, 其他串口通信的函数成功均返回0 ******************/
/*tcsetattr函数用于设置终端参数。函数在成功的时候返回0，失败的时候返回-1，并设置errno的值。参数fd为打开的终端文件描述符，参数optional_actions用于控制修改起作用的时间，而结构体termios_p中保存了要修改的参数。optional_actions可以取如下的值。
TCSANOW：不等数据传输完毕就立即改变属性。
TCSADRAIN：等待所有数据传输结束才改变属性。
TCSAFLUSH：清空输入输出缓冲区才改变属性。*/
if (tcsetattr(comport->fd, TCSANOW, &new_termios) != 0)
{
	printf("%s, tcsetattr failed:%s\n", __func__, strerror(errno));
	return -5;
}
printf("Comport init successfully .........\n");
return 0;
}

/***************************************发送数据******************************/
int comport_send(comport_t *comport, char *data, int bytes)
{
	char      *ptr;
	int       rv = -1;      //实际写入大小rv
	int       left_bytes = 0;
	int       wrlen = 0;
	char      buf[512];
	if (!comport || !data || bytes <= 0)
	{
		printf("%s, invalid parameter.\n", __func__);
		return -1;
	}

	ptr = data;
	left_bytes = bytes;

	while (left_bytes > 0)
	{
		wrlen = left_bytes > comport->frag_size ? comport->frag_size : left_bytes;   //数据个数

		/*********************write写入buf中********************/
		rv = write(comport->fd, ptr, wrlen);
		if (rv <= 0)
		{
			printf("write to comport[%d] failed:%s\n", comport->fd, strerror(errno));
			return -2;
		}
		printf("write to comport[%d] ok!!\n", comport->fd);
		ptr += rv;
		left_bytes -= rv;
	}
	return rv;
}


/*******************************************数据接收***************************************/
int comport_recv(comport_t *comport, char *buf, int size, int timeout)
{
	int                rv;
	fd_set           rfds;
	struct timeval  time_out;
	struct timeval  *time;
	char            *ptr;
	if (!comport || !buf || size <= 0)
	{
		printf("%s, invalid parameter.\n", __func__);
		return -1;
	}
	printf("fd=%d timeout:%d\n", comport->fd, timeout);
	ptr = buf;

	FD_ZERO(&rfds);
	FD_SET(comport->fd, &rfds);
	time_out.tv_sec = 0;
	time_out.tv_usec = timeout * 1000;
	time = timeout > 0 ? &time_out : NULL;
	while (1)
	{
		rv = select(comport->fd + 1, &rfds, NULL, NULL, time);
		if (rv < 0)
		{
			printf("select failed :%s\n", strerror(errno));
			break;
		}

		else if (rv > 0)
		{
			rv = read(comport->fd, ptr, size);
			if (rv <= 0)
			{
				printf("%s, read failure:%s\n", __func__, strerror(errno));
				return -1;
			}
			ptr += rv;
			size -= rv;
		}
		else
		{
			printf("timeout\n");
			break;
		}
	}
	return rv;
}


/**************************************端口关闭*************************************/
int comport_close(comport_t *comport)
{
	int	closere;
	if (!comport)
	{
		printf("%s() get invalid input arguments \n", __func__);
		return -1;
	}
	if (comport->fd >= 0)
	{
		printf("bye!\n");
		close(comport->fd);
	}
	comport->fd = -1;
	return 0;
}