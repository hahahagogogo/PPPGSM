#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <termios.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <libgen.h>
#include <sys/ioctl.h>
#include <signal.h>

#include"comport.h"

int           g_stop = 0;

void usage(char *name);
void sighandler(int signum);

int main(int argc, char **argv)
{
	int              opt = 0;
	int              rv = -1;
	int              i;
	fd_set          rset;
	comport_t       comport;
	long            baudrate = 9600;
	char            *settings = "8N1N";
	char            *devname = NULL;
	char            sbuf[64];     //发送缓存
	char            rbuf[512];   //接受缓存

	struct option long_options[] = {
		{ "device", required_argument, NULL, 'd' },
		{ "baudrate", required_argument, NULL, 'b' },
		{ "settings", required_argument, NULL, 's' },
		{ NULL, 0, NULL, 0 }
	};
	/*****************************************getopt_long解析长选项的功能*****************************************/
	while ((opt = getopt_long(argc, argv, "d:b:s:", long_options, NULL)) != -1)
	{
		switch (opt)
		{
		case 'd':
			devname = optarg;
			break;

		case 'b':
			baudrate = atoi(optarg);
			break;

		case 's':            /*  Default settings as 8N1N */
			settings = optarg;
			break;
		default:
			break;
		}
	}
	if (argc < 2 || !devname)
	{
		usage(argv[0]);
		return 0;
	}


	/*******************************************************串口打开***************************************/
	//comport_open(&comport, /dev/ttyUSB0, 115200, 8N1N)
	if (comport_open(&comport, devname, baudrate, settings) < 0)
	{
		printf("Failed to open %s with baudrate %d, %s. rv=%d\n", devname, baudrate, settings, rv);
		return -1;
	}
	else
	{
		printf("will send recvie!!\n");
	}
	signal(SIGINT, sighandler);


	/*****************************************************多路复用*****************************************/
	while (!g_stop)
	{
		FD_ZERO(&rset);		/*将rset清零使集合中不含任何fd*/
		FD_SET(STDIN_FILENO, &rset);/*设置stdout，使集合中包含stdout*/
		FD_SET(comport.fd, &rset);	/*将comport.fd加入rset集合*/
		rv = select(comport.fd + 1, &rset, NULL, NULL, 0);

		/********************************************非大于0一直阻塞，到有信号来******************************************/
		if (rv < 0)
		{
			continue;
		}

		if (rv == 0)
		{
			printf("Time Out \n");
			comport_close(&comport);
		}
		/****************************************************有事件发生********************************************/
		if (FD_ISSET(STDIN_FILENO, &rset))
		{
			printf("input from stdin\n");
			memset(sbuf, 0, sizeof(sbuf));
			/* Linux 下回车换行为"\n", 而我们串口发送最后为"\r" */
			fgets(sbuf, sizeof(sbuf), stdin);
			i = strlen(sbuf);
			printf("the sbuf have %d bit\n", i);
			sbuf[i - 1] = '\r';
			//       strcpy(&sbuf[i-1], "\r"); 等价

			/****************************************************发送数据***************************************************/
			if (comport_send(&comport, sbuf, strlen(sbuf)) < 0)
			{
				printf("comport send failed\n");
				goto cleanup;
			}
			else
			{
				printf("comport send ok!!! \n");
			}
		}

		if (FD_ISSET(comport.fd, &rset))
		{
			memset(rbuf, 0, sizeof(rbuf));
			/****************************************************接收数据***************************************************/
			rv = comport_recv(&comport, rbuf, sizeof(rbuf), 50);
			if (rv < 0)
			{
				printf("comport recv failed:%s\n", strerror(errno));
				break;
			}
			printf("comport recv ok!!!!rbuf is %s\n", rbuf);
		}
	}
	/****************************************************关闭串口**************************************************/
cleanup:
	comport_close(&comport);
	printf("comport close Bye!\n");
	return 0;
}

///****************************************************内联函数*****************************************************/
void usage(char *name)
{
	char *progname = NULL;
	char *ptr = NULL;

	ptr = strdup(name);
	progname = basename(ptr);
	printf("Usage1: comport -d <device> [-b <baudrate>][-s <settings>] [-x]\n");
	printf(" -d[device  ]  device name\n");
	printf(" -b[baudrate]  device baudrate (115200, 57600, 19200, 9600), default is 115200\n");
	printf(" -s[settings]  device settings as like 8N1N(default setting)\n");
	printf(" - data bits: 8, 7\n");
	printf("- parity: N=None, O=Odd, E=Even, S=Space\n");
	printf(" - stop bits: 1, 0\n");
	printf(" - flow control: N=None, H=Hardware, S=Software, B=Both\n");
	free(ptr);
	return;
}

/*********************************************** 捕捉CTRL+C ***********************************************/
void sighandler(int signum)
{
	if (signum == SIGINT)
	{
		g_stop = 1;
	}
}