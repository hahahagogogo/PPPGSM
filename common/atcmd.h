/********************************************************************************
 *      Copyright:  (C) 2022 guoyi 
 *                  All rights reserved.
 *
 *       Filename:  atcmd.h
 *    Description:  This head file 
 *
 *        Version:  1.0.0(09/05/21)
 *         Author: guoyi <675088383@qq.com>
 *      ChangeLog:  1, Release initial version on "22/04/10 14:23:00"
 *                 
 ********************************************************************************/

#ifndef __ATCMD_H__
#define __ATCMD_H__

#include <stdio.h>
#include <stddef.h>
#include <string.h>

#include "comport.h"

extern int send_atcmd(comport_t *comport, char *atcmd, char *buf, int buf_size, int timeout);

/***************************************************************************************************************
 * 功能:
 * 如果传入atreply, start_key, end_key, buf   : 再传入字符串中找到start_key与end_key之间的字符串返回出去  
 * 如果传入atreply, start_key, end_key, NULL  : 则负责查找该区间的字符串, 不将找到的字符串返回出去
 * 如果传入atreply, start_key, NULL, NULL     : 则只负责查找start_key这个字符串
 *
 * 参数:
 * atreply      :   要解析的字符串
 * start_key    :   需要返回字符串的前一个或多个索引
 * end_key      :   需要返回字符串的后一个或多个索引
 * buf          :   返回的字符串
 * size         :   防止溢出
 *
 * 返回值:
 * rv = 0 成功;     rv <0 失败;     rv = -1 输入参数不正确;     rv = -2 没找到start_key;    rv = -3 没找到end_key
 *
 *************************************************************************************************************/ 

extern int at_fetch(char *atreply, char *start_key, char *end_key, char *rbuf, int size);


#endif 

