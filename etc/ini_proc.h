#include "logger.h"

#ifndef __INI_PROC_H__
#define __INI_PROC_H__

typedef struct apn_s 
{
    char        apn[64];            /*  拨号apn    */
    char        uid[64];            /*  拨号用户名 */
    char        pwd[64];            /*  拨号密码   */
} apn_t; 

typedef struct conf_s 
{
    char    ifaces[64];
    char    pingaddr[128];      /*  ping 的地址      */
    int     timeout;            /*  每隔多久ping一次 */
    char    found_modname[64];
    int     baudrate;
    apn_t   apne;  
} conf_t; 

typedef struct mod_hwinfo_s 
{
    char    manufacturer[128];         //制造商,                 AT+CGMI
    char    module_name[128];          //模型识别(型号),		 AT+CGMM 
    char    sfver[128];				   //软件版本修订标识,       AT+CGMR
    char    imei[128];                 //IMEI号(手机序列号),     AT+CGSN
    char    cimi[128];				  //国际移动用户识别码,      AT+CIMI	 
} mod_hwinfo_t; 

enum
{
    STATUS_UNKNOWN,
    STATUS_ATOK,
    STATUS_MODULE,
    STATUS_SIMOK,
    STATUS_CSQOK,
    STATUS_REGOK,
    STATUS_IMSIOK,
};

typedef struct regist_s
{
    char    mcc[16];
    char    mnc[16];
    int     csq_rssi;                
    int     creg;
    int     status;                    
    int     found_apn;

} regist_t;

typedef struct ppp_ctx_s
{
    int             ppp_status;                
    int             action;
    char            devname[5][32];
    conf_t          conf;
    logger_t        log;
    apn_t           apne;
    mod_hwinfo_t    mod_hwinfo;
    regist_t        regist;

}ppp_ctx_t;


extern int ini_parser(const char *conf_file, ppp_ctx_t *ppp_ctx);

#endif 
