/*************************************************************************************
 *
 *		功能:	通过mcc和mnc查找出apn。如果该节点有user和password就获取出来
 *
 * 	    参数说明：	
 * 	    
 * 	    	mcc :	通过传进来的移动国家码来查找所需节点；
 *
 * 	    	mnc :	通过传进来的移动网络码来查找所需节点；
 *
 * 		    apn :	存放获取节点的apn；
 *
 * 	       user :	存放获取节点的user；
 * 		
 * 	   password :	存放获取节点的password；	
 *
 *
 * ***********************************************************************************/


#ifndef _PARSE_XML_H
#define _PARSE_XML_H

//int query_apn_xml(char *mcc, char *mnc, char *apn, char *user, char *password);
int query_apn_xml(char *mcc, char *mnc, apn_t *apne);

#endif
