#include <stdio.h>
#include <stdlib.h>
#include "iniparser.h"
#include "ini_proc.h"
#include "logger.h"


int ini_parser(const char *conf_file, ppp_ctx_t  *ppp_ctx)
{
    dictionary     *ini;
    const char     *str;
    int             val;
    conf_t         *conf;
    logger_t       *log;
    apn_t          *apne;

    if( !conf_file || !ppp_ctx ) 
    {
        fprintf(stderr, "ERROR: parser configure file or ctx is NULL\n");
        return 0;
    }

    conf = &ppp_ctx->conf;
    log = &ppp_ctx->log;
    apne = &conf->apne;

    ini = iniparser_load(conf_file);
    if( !ini ) 
    {
        fprintf(stderr, "ERROR: cannot parse file: '%s'\n", conf_file);
        return -1;
    }

    str = iniparser_getstring(ini, "LOGGER:filename", NULL);
    if( !str )
    {
        printf("find filename failure\n");
        return -2;
    }
    strncpy(log->file, str, sizeof(log->file));

    val = iniparser_getint(ini, "LOGGER:loglevel", -1);     /*  如果ini配置文件中有loglevel 后面没有值, 则返回0, 如果文件中没有loglevel 则返回-1(第三个参数值) */
    if ( val <= 0 )
    {
        printf("find loglevel failure\n");
        return -3;
    }
    log->level = val;

    val = iniparser_getint(ini, "LOGGER:logsize", -1);
    if( val <= 0 )
    {
        printf("find logsize failure\n");
        return -4;
    }
    log->size = val;


    /*-----------------------------------------------------------------------------
     *  日志系统的初始化
     *-----------------------------------------------------------------------------*/
    if( log_open(log, ppp_ctx->log.file, ppp_ctx->log.level, ppp_ctx->log.size) < 0 ) 
    {   
        fprintf(stderr, "initialise logger system failure, \n");
        return -1; 
    }
    log_dbg("Logging system initialized successfully\n");


    str = iniparser_getstring(ini, "NETWORK:ifaces", NULL);
    if( !str )
    {
        log_err("find ifaces failure\n");
        return -5;
    }
    strncpy(conf->ifaces, str, sizeof(conf->ifaces));
    log_dbg("find ifaces successfully :%s\n", conf->ifaces);

    str = iniparser_getstring(ini, "NETWORK:pingaddr", NULL);
    if( !str )
    {
        log_err("find pingaddr failure\n");
        return -6;
    }
    strncpy(conf->pingaddr, str, sizeof(conf->pingaddr));
    log_dbg("find pingaddr successfully :%s\n", conf->pingaddr);

    val = iniparser_getint(ini, "NETWORK:interval", -1);
    if( val <= 0 )
    {
        log_err("find interval failure \n");
        return -7;
    }
    conf->timeout = val;
    log_dbg("find timeout successfully :%d\n", conf->timeout);

    str = iniparser_getstring(ini, "MODULE:modname", NULL);
    if( !str )
    {
        log_err("find modname failure\n");
        return -8;
    }
    strncpy(conf->found_modname, str, sizeof(conf->found_modname));
    log_dbg("find modname successfully :%s\n", conf->found_modname);

    val = iniparser_getint(ini, "MODULE:baudrate", -1);
    if( val <= 0 )
    {
        log_err("find baudrate failure\n");
        return -9;
    }
    conf->baudrate = val;
    log_dbg("find baudrate successfully :%d\n", conf->baudrate);

    str = iniparser_getstring(ini, "APN:apn", NULL);
    if( str )
    {
        strncpy(apne->apn, str, sizeof(apne->apn));
        log_dbg("find apn successfully : %s\n", apne->apn);
    }
    else
    {
        log_warn("find apn failure\n");
    }

    str = iniparser_getstring(ini, "APN:uid", NULL);
    if( str )
    {
        strncpy(conf->apne.uid, str, sizeof(conf->apne.uid));
    }
    else 
    {
        log_warn("find uid failure\n");
    }

    str = iniparser_getstring(ini, "APN:pwd", NULL);
    if( str )
    {
        strncpy(conf->apne.pwd, str, sizeof(conf->apne.pwd));
    }
    else
    {
        log_warn("find pwd failure\n");
    }

    return 0;

}

