#include <libxml/parser.h>
#include <string.h>
#include <unistd.h>

/*************************************************************************************
*        功能:    通过mcc和mnc查找出apn。如果该节点有user和password就获取出来
*         参数说明：
*typedef struct apn_s 
{
*   char        apn[64];           拨号apn    
*   // char        uid[64];           拨号用户名
*	//char        pwd[64];           拨号密码 
	char        mcc[16];		    通过传进来的移动国家码来查找所需节点；
	char        mnc[16];			通过传进来的移动网络码来查找所需节点；
*	} apn_t;
   apn  存放获取节点的apn；

* ***********************************************************************************/


#include "ini_proc.h"

int query_apn_xml(char *mcc, char *mnc, apn_t *apne)
{
	xmlDocPtr	pdoc;			//读出XML文档指针doc
	xmlNodePtr	proot, curnode; //节点1 节点2
	xmlChar		*attrmcc;
	xmlChar		*attrmnc;
	xmlChar		*attrapn;
	xmlChar		*attrpassword;
	xmlChar		*attruser;
	char		*docfilename = "apns-full-conf.xml";

	/* *********************忽略空白符，必须加上，防止程序把元素前后的空白文本符号当作一个node **********************/
	xmlKeepBlanksDefault(0);

	/* *********************打开文件，libxml只能解析UTF-8格式数据 **********************/
	pdoc = xmlReadFile(docfilename, "UTF-8", XML_PARSE_RECOVER);
	if(!pdoc)
	{
		printf("error:can't open file!\n");
		return -1;
	}

	/******************获取xml文档对象的根节对象*********************/
	proot = xmlDocGetRootElement(pdoc);
	if(!proot)
	{
		printf("not proot is doc\n");
		xmlFreeDoc(pdoc);
		return -2;
	}

	/* 找到apns这个根节点 */
	if(xmlStrcmp(proot->name, BAD_CAST "apns") != 0)
	{
		printf("error:apns proot is empty!\n");
		xmlFreeDoc(pdoc);
		return -3;
	}
	printf("apns proot have find~~~\n");

	////如同标准C中的char类型一样，xmlChar也有动态内存分配，字符串操作等 相关函数。例如xmlMalloc是动态分配内存的函数；xmlFree是配套的释放内存函数；xmlStrcmp是字符串比较函数等。
	curnode = proot->xmlChildrenNode;

	/*递归查找.....*/
	while(curnode != NULL)
	{
		/*忽略空白字符*/
		xmlKeepBlanksDefault(0);

		/* 继续查找到apn这个节点 */
		if(!xmlStrcmp(curnode->name, BAD_CAST "apn"))
		{
			/* 
			函数功能	判断该节点有无mus属性 
			函数接口	xmlNodePtr xmlHasProp( xmlNodePtr cur, xmlChar *propNode）
			参数说明	cur：节点的指针 ； propNode：属性名 ； 返回值：成功为节点指针		
			*/
			if(xmlHasProp(curnode, BAD_CAST "mcc") && xmlHasProp(curnode, BAD_CAST "mnc"))	
			{

				/*获得节点的属性mnc mcc apn在一起的一起获取*/
				attrmcc = xmlGetProp(curnode, BAD_CAST "mcc");
				attrmnc = xmlGetProp(curnode, BAD_CAST "mnc");
				attrapn = xmlGetProp(curnode, BAD_CAST "apn");
				/*查找节点mcc和mnc*/
				if(!xmlStrcmp(attrmcc, BAD_CAST mcc) && !xmlStrcmp(attrmnc, BAD_CAST mnc))
				{
					strncpy(apne->apn, (char *)attrapn, strlen(attrapn));
					if(xmlHasProp(curnode, BAD_CAST "password") && xmlHasProp(curnode, BAD_CAST "user"))	
					{
						
						attrpassword = xmlGetProp(curnode, BAD_CAST "password");
						attruser = xmlGetProp(curnode, BAD_CAST "user");
						strncpy(apne->pwd, (char *)attrpassword, strlen(attrpassword));
						strncpy(apne->uid, (char *)attruser, strlen(attruser));
					}
					break;
				}
			}
		}
		/*下一个域*/
		curnode = curnode->next;

	}
	
	/*****************释放资源********************/
	xmlFreeDoc(pdoc);
	xmlCleanupParser();
	xmlMemoryDump();
	return 0;
	
}

