#!/bin/bash

#+--------------------------------------------------------------------------------------------
#|Description:  This shell script used download and compile libxml2 for ARM
#|     Author:  guanyunpeng <364521112@qq.com>
#|  ChangeLog:
#|           1, Initialize 1.0.0 on 2021.04.12
#+--------------------------------------------------------------------------------------------

PREFIX_PATH=`pwd`/install/

LYFTP_SRC=ftp://master.iot-yun.club/src/

CROSSTOOL=/opt/buildroot/arm926t/bin/arm-linux-

function msg_banner()
{
    echo ""
    echo "+-----------------------------------------------------------------------"
    echo "|  $1 "
    echo "+-----------------------------------------------------------------------"
    echo ""
}

function check_result()
{
    if [ $? != 0 ] ; then
       echo ""
       echo "+-----------------------------------------------------------------------"
       echo "|  $1 "
       echo "+-----------------------------------------------------------------------"
       echo ""
       exit ;
    fi
}

function export_cross()
{
    # export cross toolchain
    export CC=${CROSSTOOL}gcc
    export AS=${CROSSTOOL}as
    export AR=${CROSSTOOL}ar
    export LD=${CROSSTOOL}ld
    export NM=${CROSSTOOL}nm
    export RANLIB=${CROSSTOOL}ranlib
    export OBJDUMP=${CROSSTOOL}objdump
    export STRIP=${CROSSTOOL}strip

    # export cross configure 
    export CONFIG_CROSS=" --build=i686-pc-linux --host=arm-linux "

    # Clear LDFLAGS and CFLAGS
    export LDFLAGS=
    export CFLAGS=
}


function compile_libxml2()
{
    SRC_NAME=libxml2-2.9.12
    PACK_SUFIX=tar.gz

    if [ -f ${PREFIX_PATH}/lib/libxml2.so ] ; then
        msg_banner "$SRC_NAME already compile and installed"
        return 0;
    fi

    msg_banner "Start donwload $SRC_NAME "
    if [ ! -f ${SRC_NAME}.${PACK_SUFIX} ] ; then
        #wget ftp://xmlsoft.org/libxml2/libxml2-2.9.12.tar.gz
        wget ${LYFTP_SRC}/${SRC_NAME}.${PACK_SUFIX}
        check_result "ERROR: download ${SRC_NAME} failure"
    fi

    tar -xzf ${SRC_NAME}.${PACK_SUFIX}
    cd ${SRC_NAME}

    msg_banner "Start cross compile $SRC_NAME "

    ./configure --prefix=${PREFIX_PATH} ${CONFIG_CROSS} --without-python

    check_result "ERROR: configure ${SRC_NAME} failure"

    make && make install
    check_result "ERROR: compile ${SRC_NAME} failure"

    cd -
}

export_cross

compile_libxml2


